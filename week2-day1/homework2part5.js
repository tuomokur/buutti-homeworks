let balance = 100;
let isActive = true;
let checkBalance = true;

// Check balance. Yes/No?
if(checkBalance === false){
    console.log('Have a nice day');
}
// Is account active and balance > 0
if(checkBalance === true && isActive === true && balance > 0){
    console.log(balance);
}
// Is account active?
if(checkBalance === true && isActive === false){
    console.log('Your account is not active');
}
// Account balance check
if(checkBalance === true && isActive === true && balance === 0){
    console.log('Your account is empty');
}
if(checkBalance === true && isActive === true && balance < 0){
    console.log('Your balance is negative');
}

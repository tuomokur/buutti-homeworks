let lang = String(process.argv[2]);

const es = '¡Hola Mundo!';
const swe = 'Hejsan värld';
const pc = '01001000 01100101 01101100 01101100 01101111 00100001';

if(lang === 'es'){
	lang = es;
}else if(lang === 'swe'){
	lang = swe;
}else if(lang === 'pc'){
	lang = pc;
}else{
	lang = 'Hello world!';
}

console.log(lang); 
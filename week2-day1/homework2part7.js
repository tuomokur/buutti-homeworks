let name_1 = String(process.argv[2]);
let name_2 = String(process.argv[3]);
let name_3 = String(process.argv[4]);

let a = name_1.length; //short variable names for sake of typing
let b = name_2.length;
let c = name_3.length;

let longest = '';
let secondLongest = '';
let thirdLongest = '';

if(a > b && a > c && b > c ){   //A, B, C)
    longest = name_1;
    secondLongest = name_2;
    thirdLongest = name_3;
}
if(a > b && a > c && c > b){ //(A, C, B)
    longest = name_1;
    secondLongest = name_3;
    thirdLongest = name_2;
}
if (b > a && b > c && a > c){ //(B, A, C)
    longest = name_2;
    secondLongest = name_1;
    thirdLongest = name_3;
}
if(b > c && b > a && c > a){ //(B, C, A)
    longest = name_2;
    secondLongest = name_3;
    thirdLongest = name_1;
}
if(c > a && c > b && a > b){ //(C, A, B)
    longest = name_3;
    secondLongest = name_1;
    thirdLongest = name_2;
}
if(c > b && c > a && b > a){ //(C, B, A)
    longest = name_3;
    secondLongest = name_2;
    thirdLongest = name_1;
}


console.log(longest, secondLongest, thirdLongest);


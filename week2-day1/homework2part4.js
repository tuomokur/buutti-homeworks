const input = Number(process.argv[2]);

const month = input - 1; 
const daysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];

console.log('Month ' + (month + 1)  + ' has ' + daysInMonth[month] + ' days');

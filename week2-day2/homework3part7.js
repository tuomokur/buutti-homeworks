// Randomize order of array
const array = [1, 2, 4, 4, 5, 6, 7, 8, 9, 10];
const randomizedArray = array.sort(() => Math.random() - 0.5);


console.log(randomizedArray);
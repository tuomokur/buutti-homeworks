// isPalindrome
let word =  String(process.argv[2]);

let reversed = word.split("").reverse().join("");

if(word === reversed){
    console.log('Yes ' + word + ' is a palindrome');
}else if(word !== reversed){
    console.log('No ' + word + ' is not a palindrome');
}

// console.log(reversed);